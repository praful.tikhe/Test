//
//  TestApp.swift
//  Test
//
//  Created by M2COMAC00007 on 30/05/23.
//

import SwiftUI

@main
struct TestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
